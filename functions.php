<?php
if (!function_exists('mysql_escape_string')) {
function mysql_escape_string($string){return mysqli_escape_string($string);}
}


include_once('includes/photologo-options.php');
include_once('includes/services.php');

// Visual Composer Extensions
include_once('includes/vc-checkout-form.php');
include_once('includes/vc-verify.php');
include_once('includes/vc-tracking.php');
include_once('includes/vc-contact-form.php');
include_once('includes/vc-contact-checkout-form.php');
include_once('includes/vc-recent-blog-gallery.php');
include_once('includes/vc-phototalk-header.php');
include_once('includes/vc-story-component.php');
include_once('includes/vc-story-posts-component.php');


add_action('after_setup_theme', 'uncode_language_setup');
function uncode_language_setup()
{
	load_child_theme_textdomain('uncode', get_stylesheet_directory() . '/languages');
}

function theme_enqueue_styles()
{
	global $apiBaseUrl, $urlPayment, $wp_query;

  $themeUrl = get_stylesheet_directory_uri();
	$production_mode = ot_get_option('_uncode_production');
	$resources_version = ($production_mode === 'on') ? null : rand();
	$parent_style = 'uncode-style';
	$child_style = array('uncode-custom-style');
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/library/css/style.css', array(), $resources_version);
	wp_enqueue_style('child-style',			$themeUrl . '/css/main.css', $child_style, $resources_version);
	wp_enqueue_script('child-script',		$themeUrl . '/js/all.js', array('jquery'), $resources_version, true);
	wp_localize_script('child-script', 'myAjax', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'apibaseurl' => $apiBaseUrl,
		'urlPayment' => $urlPayment,
		'query' => $wp_query->query,
		'nonce' => wp_create_nonce('myAjax-nonce'),
	));

}
add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function print_fb_tracking_code() {
    ?>
        <!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '167288286977475');
		fbq('track', 'PageView');
		</script>
		<noscript>
		<img height="1" width="1"
		src="https://www.facebook.com/tr?id=167288286977475&ev=PageView
		&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->
	    <?php if (!is_page('checkout') && !is_page('verify')):?>
			<script>fbq('track', 'ViewContent');</script>
		<?php endif;?>
	<?php
}
add_action('wp_head', 'print_fb_tracking_code');

function photologo_rewrite_tags() {
	add_rewrite_tag('%token%', '([^&]+)');
}
add_action('init', 'photologo_rewrite_tags', 10, 0);

function photologo_add_rewrite_rules() {
    add_rewrite_rule(
        '^track/([^/]*)/?$',
	    'index.php?pagename=track&token=$matches[1]',
	    'top'
    );
}
add_action( 'init', 'photologo_add_rewrite_rules' );

function phototalk_create_story_post_type(){
	register_post_type( 'story',
    array(
      'labels' => array(
        'name' => __( 'All Stories' ),
        'singular_name' => __( 'Story' ),
      ),
			'supports' => array('thumbnail','editor','author','title','comments','revisions'),
      'public' => true,
      'has_archive' => true,
			'rewrite' => array('slug' => 'stories'),
    )
  );
}

if(function_exists(phototalk_create_story_post_type)){
	add_action( 'init', 'phototalk_create_story_post_type');
}

//AJAX Load More
function be_ajax_load_more(){

	check_ajax_referer('myAjax-nonce', 'nonce');

	$args = isset( $_POST['query'] ) ? array_map( 'esc_attr', $_POST['query'] ) : array();
	$args['post_type'] = isset( $args['post_type'] ) ? esc_attr( $args['post_type'] ) : 'story';
	$args['paged'] = esc_attr( $_POST['page'] );
	$args['post_status'] = 'publish';

	ob_start();
	$loop = new WP_Query( $args );
	if( $loop->have_posts() ): while( $loop->have_posts() ): $loop->the_post();
		be_post_summary();
	endwhile; endif; wp_reset_postdata();
	$data = ob_get_clean();
	wp_send_json_success( $data );
	wp_die();
}
add_action('wp_ajax_be_ajax_load_more', 'be_ajax_load_more');
add_action('wp_ajax_nopriv_be_ajax_load_more', 'be_ajax_load_more');
