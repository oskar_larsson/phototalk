<?php
/*
* Photologo Options Page
*/

if(!class_exists('Photologo_Options')) 
{
	class Photologo_Options {
		static $option_name = 'photologo_opts';

		static function init() {
			add_action('admin_init', array(__CLASS__, 'admin_init'));  // Used for registering settings
			add_action('admin_menu', array(__CLASS__, 'add_page'));    // Creates admin menu page

			// set global variable
			global $apiBaseUrl, $urlPayment;
			$options = get_option(self::$option_name);
			$apiBaseUrl = $options['api_base_url'];
			$urlPayment = $options['url_payment'];
		}

		// Register settings array
		static function admin_init() {
			register_setting('photologo_options', self::$option_name, array(__CLASS__, 'validate_options'));
		}

		static function validate_options($input) {

			$valid = array();
			$valid['api_base_url'] = $input['api_base_url'];
			$valid['url_payment'] = $input['url_payment'];

			return apply_filters('photologo_options_validator', $valid, $input);
		}

		// Initialize admin page
		static function add_page() {
			$photologo_page = add_options_page('Photologo Options', 'Photologo Options', 'manage_options', 'photologo_options', array(__CLASS__, 'options_do_page'));
		}

		// Generate admin options page
		static function options_do_page() {

			$options = get_option(self::$option_name);
			?>
			<div class="wrap">
				<div id="icon-themes" class="icon32"></div>
				<h2>Photologo Options</h2>

				<form method="post" action="options.php">

					<div class="postbox">
						<?php settings_fields('photologo_options'); ?>
						<table class="form-table">
							<tr valign="top"><th scope="row"><?php _e('API Base Url:', 'photologo_lang'); ?></th>
								<td><input id="api_base_url" type="text" name="<?php echo self::$option_name ?>[api_base_url]" value="<?php echo esc_attr($options['api_base_url']); ?>" /></td>
							</tr>
							<tr valign="top"><th scope="row"><?php _e('PP MicroService Url:', 'photologo_lang'); ?></th>
								<td><input id="url_payment" type="text" name="<?php echo self::$option_name ?>[url_payment]" value="<?php echo esc_attr($options['url_payment']); ?>" /></td>
							</tr>
							<?php do_action('photologo_option_fields');?>
						</table>
					</div>
						
					<p class="submit">
						<input type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes', 'photologo_lang') ?>" />
					</p>
				</form>

				<style>
					/* .postbox mods */
					.postbox {
						padding: 10px;
					}
					.postbox input[type="text"] {
						width: 400px;
					}
				</style>
			</div> <!-- End wrap -->
			<?php
		}
	}
}

if(class_exists('Photologo_Options'))
{
	Photologo_Options::init();
}
