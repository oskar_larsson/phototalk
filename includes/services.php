<?php

function get_fulfillment($token) {
	global $apiBaseUrl;

	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $apiBaseUrl . '/fulfillment/?client_token=' . $token,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		return false;
	} else {
		$response = json_decode($response, true);
		if (isset($response['detail']) && $response['detail'] == 'Not found.') {
			return array('status' => 'invalid');
		} else if (isset($response[0]['detail']) && $response[0]['detail'] == 'unfulfilled') {
			return array('status' => 'unfulfilled');
		} else if (isset($response[0]['concept_set'])) {
			return array('status' => 'fulfilled', 'items' => $response);
		} else {
			return false;
		}
	}
}

function get_payment_details($paymentId) {
	global $urlPayment;

	if(isset($paymentId)) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $urlPayment . '/api/getPaymentDetails',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode(array(
				"paymentId" => $paymentId)),
			CURLOPT_HTTPHEADER => array("content-type: application/json")
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			$error = $err;
		} else {
			$details = json_decode($response, true);

			if($details['httpStatusCode'] != 200) {
				try {
					$error = $details['response']['message'];
				} 
				catch(Exception $e) {
					$error = 'There was an error while retrieving your payment detail. Please try again.';
				}
			}
		}
	} else {
		$error = "Requested payment ID was not found.";
	}

	if ($error) {
		return array('error' => $error);
	} else {
		return $details;
	}
}

function execute_payment($data) {
	global $urlPayment;

	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $urlPayment . '/api/executePayment',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => json_encode($data),
		CURLOPT_HTTPHEADER => array("content-type: application/json")
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		$error = $err;
	} else {
		$details = json_decode($response, true);

		if($details['httpStatusCode'] != 200) {
			try {
				$error = $details['response']['message'];
			} 
			catch(Exception $e) {
				$error = 'There was an error while executing your payment. Please try again.';
			}
		}
	}

	if ($error) {
		return array('error' => $error);
	} else {
		return $details;
	}
}