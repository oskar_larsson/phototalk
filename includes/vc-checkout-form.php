<?php
/*
* Add-on Name: Photologo Checkout Form
*/
if(!class_exists('Photologo_Checkout_Form_Module'))
{
	class Photologo_Checkout_Form_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('photologo_checkout_form', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {
			wp_register_script('form-validator', get_stylesheet_directory_uri() . '/vendor/jquery.form-validator.min.js', array('jquery'), false, true);
			wp_register_script('paypal', 'https://www.paypalobjects.com/api/checkout.js', array('jquery'), false, true);
		}

		static function print_script() {
			if ( ! self::$add_script )
				return;

			wp_enqueue_script('form-validator');
			wp_enqueue_script('paypal');
		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
				'confirm_url' => '',
				'cancel_url' => ''
			), $atts, 'photologo_checkout_form' ) );

			$return_url = get_permalink() . '?v=1';

			global $_REQUEST;

			$v = $_REQUEST['v'];
			$paymentId = $_REQUEST['paymentId'];

			$paid = false;

			// if returned by paypal
			if($v == '1') {
				$paid = true;
				$confirm_url = $confirm_url . '?paymentId=' . $paymentId;

				$details = get_payment_details($paymentId);

				if(isset($details['error'])) {
					$error = $details['error'];
				} else {
					try {
						$orderDetails = self::get_order($details);
					}
					catch(Exception $e) {
						$error = 'There was an error while retrieving order summary. Please try again.';
					}
				}
			}

			ob_start();
		?>

		<div class="photologo-checkout-form-wrapper <?php echo $paid ? 'checkout-success' : '' ?>">
			<div class="step-wrapper style-dark text-center">
				<div class="white-logo-wrapper"></div>
				<h2>Order Photopolish</h2>
				<p><i class="icon-down"></i></p>
			</div>

			<div class="form-wrapper style-light text-center">
				<div class="form-bg-container">
					<p class="photopolish-title-wrapper">ONE TIME PAY LIFETIME LICENCE</p>
				</div>
				<?php if(!$paid):?>

					<div class="form form--step3">
						<form>
							<div class="field photopolish_appicon_black">
							</div>
							<div class="field">
								<label for="name">Full Name</label>
								<input type="text" id="name" name="name" placeholder="We recommend your full name!" data-validation="required">
							</div>
							<div class="field">
								<label for="email">Email</label>
								<input type="email" id="email" name="email" placeholder="Remember to check your SPAM folder!" data-validation="email">
							</div>


							<div class="btn-container">
								<div id="ppButton"></div>
								<style id="ppButton_style"></style>
								<span class="payment-processors"></span>
							</div>

							<div class="form-submit-error"></div>

							<?php
								if (!empty($return_url)) {
									echo '<input type="hidden" name="returnUrl" value="' . esc_url($return_url) . '" />';
								} else {
									echo '<input type="hidden" name="returnUrl" value="' . home_url() . '" />';
								}

								if (!empty($cancel_url)) {
									echo '<input type="hidden" name="cancelUrl" value="' . esc_url($cancel_url) . '" />';
								} else {
									echo '<input type="hidden" name="cancelUrl" value="' . home_url() . '" />';
								}
							?>
						</form>
					</div>

				<?php else:?>

					<?php if(isset($error)):?>

						<div class="error-block">
							<h3><?=$error;?></h3>
						</div>

					<?php else:?>

						<div class="form form--step4">
							<h3 class="heading">Final Step</h3>
							<p class="subheading">(Confirm Order Below)</p>

							<ul class="order-summary">
								<li>
									<p class="label">Thickness:</p>
									<span class="value">
									<?php if($orderDetails['thickness'] == 'any'):?>
										<span class="thickness text">Any</span>
									<?php else:?>
										<span class="thickness thickness--<?=$orderDetails['thickness']?>"></span>
									<?php endif;?>
									</span>
								</li>
								<li>
									<p class="label">Curliness:</p>
									<span class="value">
									<?php if($orderDetails['curves'] == 'any'):?>
										<span class="curliness text">Any</span>
									<?php else:?>
										<span class="curliness curliness--<?=$orderDetails['curves']?>"></span>
									<?php endif;?>
									</span>
								</li>
								<li>
									<p class="label">Signature Text:</p>
									<span class="value"><span class="text"><?=$orderDetails['text']?></span></span>
								</li>
								<li>
									<p class="label">Undertag Text:</p>
									<span class="value"><span class="text"><?=$orderDetails['undertag']?></span></span>
								</li>
							</ul>

							<a href="<?=$confirm_url?>" class="btn btn-accent">Confirm Order</a>
						</div>

					<?php endif;?>

				<?php endif;?>

			</div>
		</div>

		<?php
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Photologo Checkout Form', ''),
					'base' => 'photologo_checkout_form',
					'category' => esc_html__('Content', ''),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => 'Return Url',
							'param_name' => 'return_url'
						),
						array(
							'type' => 'textfield',
							'heading' => 'Cancel Url',
							'param_name' => 'cancel_url'
						)
					)
				));
			}
		}

		static function get_string_between($string, $start, $end){
			$string = ' ' . $string;
			$ini = strpos($string, $start);
			if ($ini == 0) return '';
			$ini += strlen($start);
			if(isset($end)) {
				$len = strpos($string, $end, $ini) - $ini;
				return substr($string, $ini, $len);
			} else {
				return substr($string, $ini);
			}
		}

		static function get_order($paymentDetails) {
			$description = $paymentDetails['transactions'][0]['item_list']['items'][0]['description'];
			$text = self::get_string_between($description, 'Name: ', ' - Undertag:');
			$undertag = self::get_string_between($description, 'Undertag: ', ' - Thickness:');
			$thickness = self::get_string_between($description, 'Thickness: ', ' - Curliness:');
			$curves = self::get_string_between($description, 'Curliness: ');
			return array(
				'thickness'        => trim($thickness),
				'curves'           => trim($curves),
				'text'             => trim($text),
				'undertag'         => trim($undertag)
			);
		}
	}
}

if(class_exists('Photologo_Checkout_Form_Module'))
{
	Photologo_Checkout_Form_Module::init();
}
?>
