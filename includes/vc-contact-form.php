<?php
/*
* Add-on Name: Photologo Contact Form
*/
if(!class_exists('Photologo_Contact_Form_Module'))
{
	class Photologo_Contact_Form_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('photologo_contact_form', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {
			wp_register_script('form-validator', get_stylesheet_directory_uri() . '/vendor/jquery.form-validator.min.js', array('jquery'), false, true);
		}

		static function print_script() {
			if ( ! self::$add_script )
				return;

			wp_enqueue_script('form-validator');
		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
				'title' => '',
				'redirection_url' => '',
				'enable_toggling' => ''
			), $atts, 'photologo_contact_form' ) );

			ob_start();
		?>

		<div class="photologo-contact-form-wrapper <?=($enable_toggling == 'yes')?'':'opened'?>">
			<form novalidate>
				<input type="hidden" name="redirection_url" value="<?=$redirection_url?>" />
				<?php if ($title):?><h2 class="photologo-contact-form-heading"><?=$title?>
					<?php if ($enable_toggling == 'yes'):?><span class="toggle-open"></span><?php endif;?>
				</h2><?php endif; ?>
				<div class="toggle-content">
					<div class="photologo-contact-form-content">
						<div class="photologo-contact-form-inline-wrapper">
							<div class="photologo-contact-inline-field">
								<span class="photologo-contact-form-field-title">Name:</span>
								<p class="photologo-contact-input-field">
									<span class="photologo-contact-form-wrap your-name">
										<input type="text" data-validation="required" name="name" id="name" value="" placeholder="Enter your name here!" />
									</span>
								</p>
							</div>
							<div class="photologo-contact-inline-field">
								<span class="photologo-contact-form-field-title">Email:</span>
								<p class="photologo-contact-input-field">
									<span class="photologo-contact-form-wrap your-email">
										<input type="email" data-validation="email" name="email" id="email" value="" placeholder="Enter your email here!" />
									</span>
								</p>
							</div>
						</div>
						<div class="photologo-contact-form-inline-wrapper">
							<div class="photologo-contact-inline-field">
								<span class="photologo-contact-form-field-title">Your Question:</span>
								<p class="photologo-contact-input-field">
									<span class="photologo-contact-form-wrap your-email">
										<textarea data-validation="required" id="question" name="question" placeholder="Want to ask us something? No problem, type it here!" ></textarea>
									</span>
								</p>
							</div>
						</div>
					</div>
					<div class="photologo-contact-inline-field">
						<p class="photologo-contact-input-field submit-wrapper">
							<input type="submit" value="Send" class="btn btn-accent photologo-contact-submit-btn">
							<span class="ajax-loader"></span>
						</p>
					</div>
					<div class="form-submit-error">
						There is an error occurred while submitting form, please try again.
					</div>
				</div>
			</form>
		</div>

		<?php
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Photologo Contact Form', ''),
					'base' => 'photologo_contact_form',
					'category' => esc_html__('Content', ''),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => 'Title',
							'param_name' => 'title',
						),
						array(
							'type' => 'textfield',
							'heading' => 'Success Redirection Page Url',
							'param_name' => 'redirection_url',
						),
						array(
							"type" => 'checkbox',
							"heading" => 'Enable Toggling' ,
							"param_name" => "enable_toggling",
							"value" => array(
								'' => 'yes'
							)
						)
					)
				));
			}
		}
	}
}

if(class_exists('Photologo_Contact_Form_Module'))
{
	Photologo_Contact_Form_Module::init();
}
?>
