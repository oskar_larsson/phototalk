<?php
/*
* Add-on Name: Phototalk Header Section
*/
if(!class_exists('Phototalk_Header_Module'))
{
	class Phototalk_Header_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('phototalk_header_section', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {

		}

		static function print_script() {
			if ( ! self::$add_script )
				return;

		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
				'bg_url' => '',
				'first_pid' => 1,
				'second_pid' => 1,
				'advent_bg_url' => ''
			), $atts, 'phototalk_header_section' ) );

			$args = array(
				'numberposts' => 5,
				'offset' => 0,
				'category' => 0,
				'orderby' => 'post_date',
				'order' => 'DESC',
				'include' => '',
				'exclude' => '',
				'meta_key' => '',
				'meta_value' =>'',
				'post_type' => 'post',
				'post_status' => 'publish',
				'suppress_filters' => true
			);

			$post = get_post( $first_pid );
			ob_start();
		?>

		<div class="phototalk-header-wrapper">
			<div class="phototalk-header-left-part" style="background: url('<?=wp_get_attachment_image_src($bg_url, "large")[0];?>') no-repeat; background-size:cover;">
				<div class="blog-container">					
					<div class="phototalk-header-section-title"><h2>"<?=$post->post_title?>"</h2></div>
					<div class="phototalk-header-section-content"><p><?=$post->post_content?></p></div>
					<span class="btn-container"><a href="<?=$post->guid?>" class="custom-link btn btn-ms btn-more-stories btn-color-xsdn btn-icon-left">READ MORE</a></span>
				</div>
			</div>
			<div class="phototalk-header-right-part" style="background: url('<?=wp_get_attachment_image_src($advent_bg_url, "large")[0];?>') no-repeat; background-size:100% 100%;">
				<div class="advent">
					<span class="btn-container"><a href="<?=$post->guid?>" class="custom-link btn btn-ms btn-more-stories btn-color-xsdn btn-icon-left">LEARN MORE</a></span>
				</div>
			</div>
		</div>

		<?php
			$output = ob_get_contents();
			ob_end_clean();
			wp_reset_query();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Phototalk Header Component', ''),
					'base' => 'phototalk_header_section',
					'category' => esc_html__('Content', ''),
					'params' => array(
						array(
							'type' => 'attach_image',
							'heading' => 'Background Image',
							'param_name' => 'bg_url',
						),
						array(
							'type' => 'textfield',
							'heading' =>'First postID',
							'param_name' => 'first_pid'
						),
						array(
							'type' => 'textfield',
							'heading' =>'Second postID',
							'param_name' => 'second_pid'
						),
						array(
							'type' => 'attach_image',
							'heading' => 'Advent Image',
							'param_name' => 'advent_bg_url',
						),
					)
				));
			}
		}
	}
}

if(class_exists('Phototalk_Header_Module'))
{
	Phototalk_Header_Module::init();
}
?>
