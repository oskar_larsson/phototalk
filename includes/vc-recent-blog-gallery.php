<?php
/*
* Add-on Name: Phototalk Recent Blog Gallery
*/
if(!class_exists('Phototalk_Recent_Blog_Gallery_Module'))
{
	class Phototalk_Recent_Blog_Gallery_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('phototalk_recent_blog_gallery', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {

		}

		static function print_script() {
			if ( ! self::$add_script )
				return;

		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
				'title' => '',
			), $atts, 'phototalk_recent_blog_gallery' ) );

			$args = array(
				'numberposts' => 5,
				'offset' => 0,
				'category' => 0,
				'orderby' => 'post_date',
				'order' => 'DESC',
				'include' => '',
				'exclude' => '',
				'meta_key' => '',
				'meta_value' =>'',
				'post_type' => 'post',
				'post_status' => 'publish',
				'suppress_filters' => true
			);

			$recent_posts = wp_get_recent_posts( $args );
			ob_start();
		?>

		<div class="phototalk-recent-blog-gallery-wrapper">
			<?php if ($title):?><h2 class="phototalk-recent-blog-gallery-heading"><?=$title?></h2><?php endif; ?>
			<div class="phototalk-recent-blog-gallery-container">
				<?php
					foreach($recent_posts as $recent):
				?>
				<div class="phototalk-recent-blog-gallery-item-wrapper" style="background: url('<?=has_post_thumbnail($recent['ID'],'thumbnail')?wp_get_attachment_url(get_post_thumbnail_id($recent['ID']),'thumbnail'):''?>') no-repeat; background-size: cover; ">
					<div class="overlay-wrapper">
						<div class="phototalk-recent-blog-gallery-title"><h2><?=$recent['post_title']?></h2></div>
						<div class="phototalk-recent-blog-gallery-content"><p><?=$recent['post_content']?></p></div>
						<span class="btn-container"><a href="<?=$recent['guid']?>" class="custom-link btn btn-xs btn-more-stories btn-color-xsdn btn-icon-left">READ MORE</a></span>
					</div>
				</div>
				<?php endforeach;?>
			</div>
		</div>

		<?php
			$output = ob_get_contents();
			ob_end_clean();
			wp_reset_query();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Phototalk Recent Blog Gallery', ''),
					'base' => 'phototalk_recent_blog_gallery',
					'category' => esc_html__('Content', ''),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => 'Title',
							'param_name' => 'title',
						)
					)
				));
			}
		}
	}
}

if(class_exists('Phototalk_Recent_Blog_Gallery_Module'))
{
	Phototalk_Recent_Blog_Gallery_Module::init();
}
?>
