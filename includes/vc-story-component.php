<?php
/*
* Add-on Name: Phototalk Story Section
*/
if(!class_exists('Phototalk_Story_Module'))
{
	class Phototalk_Story_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('phototalk_story_section', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {

		}

		static function print_script() {
			if ( ! self::$add_script )
				return;

		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
				'title' => '',
				'story_id' => '',
				'author_name' => ''
			), $atts, 'phototalk_story_section' ) );

			$args = array(
				'numberposts' => 5,
				'offset' => 0,
				'category' => 0,
				'orderby' => 'post_date',
				'order' => 'DESC',
				'include' => '',
				'exclude' => '',
				'meta_key' => '',
				'meta_value' =>'',
				'post_type' => 'post',
				'post_status' => 'publish',
				'suppress_filters' => true
			);

			$post = get_post( $story_id );
			ob_start();
		?>

		<div class="phototalk-story-wrapper">
			<div class="phototalk-story-left-part">
				<div class="blog-container">
					<div class="phototalk-story-section-title"><h2><?=$title?></span></h2></div>
					<div class="phototalk-story-section-content"><h2>"<?=$post->post_title?>"</h2></div>
					<div class="phototalk-story-section-wrapper"><p>"<?=$post->post_content?>"</p></div>
					<div class="phototalk-story-author"><h2>-<?=$author_name?></h2></div>
					<span class="btn-container"><a href="<?=$post->guid?>" class="custom-link btn btn-xl btn-more-stories btn-color-wayh btn-icon-left">VIEW MORE STORIES</a></span>
				</div>
			</div>
			<div class="phototalk-story-right-part" style="background: url('<?=has_post_thumbnail($post->ID,'large')?wp_get_attachment_url(get_post_thumbnail_id($post->ID),'large'):''?>') no-repeat; background-size: cover; ">

			</div>
		</div>

		<?php
			$output = ob_get_contents();
			ob_end_clean();
			wp_reset_query();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Phototalk Story Component', ''),
					'base' => 'phototalk_story_section',
					'category' => esc_html__('Content', ''),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => 'Title',
							'param_name' => 'title',
						),
						array(
							'type' => 'textfield',
							'heading' =>'Story ID',
							'param_name' => 'story_id'
						),
						array(
							'type' => 'textfield',
							'heading' => 'Author\'s Name',
							'param_name' => 'author_name',
						)
					)
				));
			}
		}
	}
}

if(class_exists('Phototalk_Story_Module'))
{
	Phototalk_Story_Module::init();
}
?>
