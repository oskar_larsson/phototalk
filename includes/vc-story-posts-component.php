<?php
/*
* Add-on Name: Phototalk Story Section
*/
if(!class_exists('Phototalk_Story_Posts_Module'))
{
	class Phototalk_Story_Posts_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('phototalk_story_posts_section', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {

		}

		static function print_script() {
			if ( ! self::$add_script )
				return;

		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
				'story_number' => '',
			), $atts, 'phototalk_story_posts_section' ) );

			$args = array(
				'posts_per_page'   => $story_number,
				'offset'           => 0,
				'category'         => '',
				'category_name'    => '',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'include'          => '',
				'exclude'          => '',
				'meta_key'         => '',
				'meta_value'       => '',
				'post_type'        => 'story',
				'post_mime_type'   => '',
				'post_parent'      => '',
				'author'	   => '',
				'author_name'	   => '',
				'post_status'      => 'publish',
				'suppress_filters' => true
			);

			$stories_array = get_posts( $args );
			ob_start();
			foreach ($stories_array as $key => $story) {
				setup_postdata($story);
				if($key%2==0):
		?>
		<div class="phototalk-story-container">
			<div class="phototalk-story-wrapper">
				<div class="phototalk-story-left-part">
					<div class="blog-container">
						<div class="phototalk-story-section-title"><h2><?=the_title()?></span></h2></div>
						<div class="phototalk-story-section-content"><h2>"<?=$story->post_title?>"</h2></div>
						<div class="phototalk-story-section-wrapper"><p>"<?=$story->post_content?>"</p></div>
					</div>
				</div>
				<div class="phototalk-story-right-part" style="background: url('<?=has_post_thumbnail($story->ID,'thumbnail')?wp_get_attachment_url(get_post_thumbnail_id($story->ID),'thumbnail'):''?>') no-repeat; background-size: cover; ">

				</div>
			</div>
		</div>

		<?php
				else:
		?>
		<div class="phototalk-story-container">
			<div class="phototalk-story-wrapper">
				<div class="phototalk-story-right-part" style="background: url('<?=has_post_thumbnail($story->ID,'large')?wp_get_attachment_url(get_post_thumbnail_id($story->ID),'large'):''?>') no-repeat; background-size: cover; "></div>
				<div class="phototalk-story-left-part">
					<div class="blog-container">
						<div class="phototalk-story-section-title"><h2><?=the_title()?></span></h2></div>
						<div class="phototalk-story-section-content"><h2>"<?=$story->post_title?>"</h2></div>
						<div class="phototalk-story-section-wrapper"><p>"<?=$story->post_content?>"</p></div>
					</div>
				</div>
			</div>
		</div>
		<?php
				endif;
			}
			$output = ob_get_contents();
			ob_end_clean();
			wp_reset_postdata();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Phototalk Story Posts Component', ''),
					'base' => 'phototalk_story_posts_section',
					'category' => esc_html__('Content', ''),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => 'Number of stories to load',
							'param_name' => 'story_number',
						),
					)
				));
			}
		}
	}
}

if(class_exists('Phototalk_Story_Posts_Module'))
{
	Phototalk_Story_Posts_Module::init();
}
?>
