<?php
/*
* Add-on Name: Photologo Tracking
*/
if(!class_exists('Photologo_Tracking_Module')) 
{
	class Photologo_Tracking_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('photologo_tracking', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {
			wp_register_script('form-validator', get_stylesheet_directory_uri() . '/vendor/jquery.form-validator.min.js', array('jquery'), false, true);
		}

		static function print_script() {
			if ( ! self::$add_script )
				return;

			wp_enqueue_script('form-validator');
		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
			), $atts, 'photologo_tracking' ) );

			global $wp_query;
			$token = $wp_query->query_vars['token'];
			if(isset($token)) {
				$response = get_fulfillment($token);
			}

			ob_start();
		?>

		<?php if (!isset($response['status']) || $response['status'] == 'invalid'):?>

		<div class="photologo-tracking photologo-tracking--check">
			<h2>Track your order</h2>
			<form onsubmit="return false">
				<div class="input-wrapper <?php echo isset($token)?'invalid':''?>">
					<input type="hidden" name="pageUrl" value="<?=get_permalink()?>"/>
					<input type="text" name="t" id="tracking_number" placeholder="ENTER TRACKING NUMBER HERE" value="<?=$token?>">
					<button>TRACK</button>
					<span class="error">The tracking number is invalid</span>
				</div>
			</form>
		</div>

		<?php elseif ($response['status'] == 'unfulfilled'):?>

		<div class="photologo-tracking photologo-tracking--in-progress">
			<span class="icon-stopwatch icon-stopwatch--top"></span>
			<h2>Photologo in Progress</h2>
			<p>Thanks for ordering, your Photologo is<br/>
			now in the design queue, it'll be delivered to you<br/>
			once our designers have handmade it and polished it off!</p>
			<span class="icon-stopwatch icon-stopwatch--bottom"></span>
		</div>

		<?php elseif ($response['status'] == 'fulfilled'):?>

		<div class="photologo-tracking photologo-tracking--ready">
			<div class="top-wrapper">
				<span class="icon-checked icon-checked--top"></span>
				<h2>Photologo Ready!</h2>
				<p>Your brand new Photologo is<br/>
				ready to download and use.</p>
				<span class="icon-checked icon-checked--bottom"></span>
			</div>
			<div class="downloader-wrapper">
				<?php foreach($response['items'] as $item):?>
				<div class="preview-wrapper">
					<img src="<?=$item['concept_set'][0]['image']?>" class="preview">
					<div class="btn-wrapper"><a href="<?=$item['concept_set'][0]['source']?>" class="btn-download">Download</a></div>
				</div>
				<?php endforeach;?>
			</div>
		</div>
			
		<?php endif;?>

		<?php
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Photologo Tracking', ''),
					'base' => 'photologo_tracking',
					'category' => esc_html__('Content', ''),
					'params' => array(
						// TODO: Add configurable options such as background images
					)
				));
			}
		}
	}
}

if(class_exists('Photologo_Tracking_Module'))
{
	Photologo_Tracking_Module::init();
}
?>