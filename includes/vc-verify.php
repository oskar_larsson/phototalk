<?php
/*
* Add-on Name: Photologo Verify
*/
if(!class_exists('Photologo_Verify_Module'))
{
	class Photologo_Verify_Module {
		static $add_script = false;

		static function init() {
			add_action('init', array(__CLASS__, 'register_script'));
			add_action('wp_footer', array(__CLASS__, 'print_script'));
			add_shortcode('photologo_verify', array(__CLASS__, 'print_shortcode'));
			add_action('after_setup_theme', array(__CLASS__, 'add_vc_module'), 10);
		}

		static function register_script() {
		}

		static function print_script() {
			if ( ! self::$add_script )
				return;
		}

		// Shortcode handler function
		static function print_shortcode($atts, $content = null)	{
			self::$add_script = true;

			extract( shortcode_atts( array(
			), $atts, 'photologo_verify' ) );

			global $_REQUEST;

			$paymentId = $_REQUEST['paymentId'];

			if(isset($paymentId)) {
				$details = get_payment_details($paymentId);

				if(isset($details['error'])) {
					$error = $details['error'];
				} else {
					$data = execute_payment($details);
					if(isset($data) && isset($data['error'])) {
						$error = $data['error'];
					}
				}
			}

			ob_start();
		?>

		<?php if(isset($data) && isset($data['payer'])):?>
		<div class="photologo-tracking photologo-tracking--in-progress">
			<span class="icon-stopwatch icon-stopwatch--top"></span>
			<h2>Photopolish in Progress</h2>
			<p>Thank you for ordering Photopolish. Please check your email, instructions<br/>
				 on how to download and install Photopolish will be there. Remember to<br/>
				 check your SPAM folder, and allow up to 15 minutes for the email to come<br/>
				 from ordering. - If the email does not arrive, please contact our support<br/>
				 team at support@photopolish.co - Thanks, the team at Photopolish!</p>
			<span class="icon-stopwatch icon-stopwatch--bottom"></span>
		</div>

		<script>
			fbq('track', 'Purchase', {value: '<?=$data['transactions'][0]['amount']['total']?>', currency: '<?=$data['transactions'][0]['amount']['currency']?>'});
		</script>
		<?php endif;?>

		<?php if(isset($data) && isset($data['httpStatusCode']) && $data['httpStatusCode'] == 400):?>
		<div class="photologo-tracking photologo-tracking--in-progress">
			<p>This order has already been verified. Check your inbox for information or get in touch with us via email</p>
		</div>
		<?php endif;?>

		<?php if(isset($error)):?>
		<div class="photologo-tracking photologo-tracking--in-progress">
			<p><?php print_r($error)?></p>
		</div>
		<?php endif;?>

		<?php
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}

		static function add_vc_module() {
			if (class_exists('WPBakeryVisualComposerAbstract')) {
				vc_map(array(
					'name' => esc_html__('Photologo Verify', ''),
					'base' => 'photologo_verify',
					'category' => esc_html__('Content', ''),
					'params' => array(
						// TODO: Add configurable options such as background images
					)
				));
			}
		}
	}
}

if(class_exists('Photologo_Verify_Module'))
{
	Photologo_Verify_Module::init();
}
?>
