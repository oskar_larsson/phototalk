// Include gulp
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    lesshint = require('gulp-lesshint');

gulp.task('lint', function() {
    return gulp.src('less/*.less')
        .pipe(lesshint({
          }))
        .pipe(lesshint.reporter()) // Leave empty to use the default, "stylish"
        .pipe(lesshint.failOnError()); // Use this to fail the task on lint errors
});

gulp.task('style', function () {
    return gulp.src('less/*.less')
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(minifyCSS())
        .pipe(gulp.dest('../css'));
});

gulp.task('scripts', function() {
    return gulp.src('js/*.js')
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../js'))
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('less/**/*.less', ['style']);
    gulp.watch('js/*.js', ['scripts']);
});

// Default Task
gulp.task('default', ['lint','style', 'scripts', 'watch']);
gulp.task('build', ['lint','style', 'scripts']);
