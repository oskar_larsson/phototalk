(function ($) {
"use strict";

	$(document).ready(function() {
		if($('.section--recent-blog-gallery').length > 0) {
			var $arrow = $('<i class="icon-down"></i>');
			$('.section--recent-blog-gallery').prepend($arrow);
			$arrow.click(function() {
				var h = $('.section--description').outerHeight(),
					h1 = $('.section--first-order-now').outerHeight(),
					wh = $(window).height(),
					pt = $('.section--description').offset().top,
					top = 0;

				if(h+h1 < wh) {
					top = pt - (wh - (h+h1)) / 2;
				} else {
					top = pt;
				}
				$('body').animate({scrollTop: top}, 500);
			});
		}
		if($('.section--header').length > 0) {
			var $play_btn = $('.play-video-btn');
			$play_btn.css('top',($('.section--header').outerHeight()/2-$play_btn.outerHeight()/2)+'px');
			$play_btn.css('left',($('.phototalk-header-left-part').outerWidth()/2-$play_btn.outerWidth()/2)+'px');

		}
	});

}(jQuery));
