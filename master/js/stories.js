jQuery(function($){
  if($('.section--stories-wrapper').length > 0){
  	$('.section--stories-wrapper').append( '<span class="load-more"></span>' );
  	var button = $('.section--stories-wrapper .load-more');
  	var page = 2;
  	var loading = false;
  	var scrollHandling = {
  	    allow: true,
  	    reallow: function() {
  	        scrollHandling.allow = true;
  	    },
  	    delay: 400 //(milliseconds) adjust to the highest acceptable value
  	};

  	$(window).scroll(function(){
  		if( ! loading && scrollHandling.allow ) {
  			scrollHandling.allow = false;
  			setTimeout(scrollHandling.reallow, scrollHandling.delay);
  			var offset = $(button).offset().top - $(window).scrollTop();
  			if( 2000 > offset ) {
  				loading = true;
  				var data = {
  					action: 'be_ajax_load_more',
            nonce: myAjax.nonce,
  					page: page,
  					query: myAjax.query,
  				};
  				$.post(myAjax.ajaxurl, data, function(res) {
  					if( res.success) {
              alert(page);
  						$('.section--stories-wrapper').append( res.data );
  						$('.section--stories-wrapper').append( button );
  						page = page + 1;
  						loading = false;
  					} else {
  						// console.log(res);
              alert(res);
  					}
  				}).fail(function(xhr, textStatus, e) {
  					// console.log(xhr.responseText);
  				});

  			}
  		}
  	});
  }
});
